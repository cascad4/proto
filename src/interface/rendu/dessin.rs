use crate::{commun::vecteur::{Point, Vecteur}, interface::affichage::Canvas};

use sdl2::{pixels::Color, gfx::primitives::DrawRenderer};

use super::repere::Repere;

pub fn cercle_rempli(canvas: &Canvas, centre: Point, rayon: f32, repere: &Repere, couleur: Color) {
    let (x, y) = repere.projeter(centre);
    let _ = canvas.filled_circle(x, y, repere.convertir(rayon), couleur);
    let _ = canvas.aa_circle(x, y, repere.convertir(rayon), couleur);
}

pub fn cercle(canvas: &Canvas, centre: Point, rayon: f32, repere: &Repere, couleur: Color) {
    let (x, y) = repere.projeter(centre);
    let _ = canvas.aa_circle(x, y, repere.convertir(rayon), couleur);
}

pub fn texte(canvas: &Canvas, origine: Point, texte: &str, repere: &Repere, couleur: Color) {
    let (x, y) = repere.projeter(origine);
    let _ = canvas.string(x, y, texte, couleur);
}

pub fn ligne(canvas: &Canvas, debut: Point, fin: Point, repere: &Repere, couleur: Color) {
    let (x1, y1) = repere.projeter(debut);
    let (x2, y2) = repere.projeter(fin);
    let _ = canvas.aa_line(x1, y1, x2, y2, couleur);
}

pub fn bezier(canvas: &Canvas, p1: Point, p2: Point, p3: Point, p4: Point, repere: &Repere, couleur: Color) {
    let (x1, y1) = repere.projeter(p1);
    let (x2, y2) = repere.projeter(p2);
    let (x3, y3) = repere.projeter(p3);
    let (x4, y4) = repere.projeter(p4);

    let _ = canvas.bezier(&[x1, x2, x3, x4], &[y1, y2, y3, y4], 10, couleur);
}

pub fn multiligne(canvas: &Canvas, p1: Point, p2: Point, p3: Point, p4: Point, repere: &Repere, couleur: Color, epaisseur: f32) {
    let (x1, y1) = repere.projeter(p1);
    let (x2, y2) = repere.projeter(p2);
    let (x3, y3) = repere.projeter(p3);
    let (x4, y4) = repere.projeter(p4);
    let rayon = repere.convertir(epaisseur / 2.);
    let _ = canvas.filled_circle(x1, y1, rayon, couleur);
    let _ = canvas.thick_line(x1, y1, x2, y2, 2 * rayon as u8, couleur);
    let _ = canvas.filled_circle(x2, y2, rayon, couleur);
    let _ = canvas.thick_line(x2, y2, x3, y3, 2 * rayon as u8, couleur);
    let _ = canvas.filled_circle(x3, y3, rayon, couleur);
    let _ = canvas.thick_line(x3, y3, x4, y4, 2 * rayon as u8, couleur);
    let _ = canvas.filled_circle(x4, y4, rayon, couleur);
}

