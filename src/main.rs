pub mod commun;
pub mod jeu;
pub mod interface;
pub mod simulation;
pub mod stats;

use interface::affichage::Ecran;
use jeu::jeu::Jeu;

use sdl2::gfx::framerate::FPSManager;


fn main() {
    let mut ecran = Ecran::nouveau("Cascad4", 720, 480);
    let mut jeu = Jeu::nouveau();

    jeu.initialiser();

    let mut fps = FPSManager::new();
    let _ = fps.set_framerate(50);

    let mut continuer = true;
    while continuer {
        ecran.nettoyer();
        continuer = jeu.actualiser_graphique(&ecran);
        ecran.actualiser();
        fps.delay();
    }
}
