use std::f32::consts::PI;

use interface::{affichage::Ecran, dessiner::dessiner_pion_suivant};
use rand::prelude::*;

pub mod commun;
pub mod jeu;
pub mod interface;
pub mod simulation;

use commun::{objet::Pion, vecteur::{Vecteur, Point}, forme::{Forme, SolHorizontal, DemiDroiteVerticaleBasse}, joueur::Joueur};
use sdl2::{gfx::framerate::FPSManager, pixels::Color, event::Event, keyboard::{Keycode, Scancode}, mouse::MouseButton};
use simulation::{simulation::Simulation, modelisation::modele::ModelePion};

use crate::{interface::dessiner::{dessiner_pions, dessiner_formes, dessiner_alignements}, jeu::alignement::alignements};


fn ajouter_pion(sim: &mut Simulation, id: usize, x: f32, y: f32, j: bool) {
    sim.ajouter_pion(Pion {
        id,
        joueur: Joueur::from(j),
        position: Vecteur { x, y },
        vitesse: Vecteur::NUL,
        rayon: 0.0195,
    });
}

fn ajouter_cadre(sim: &mut Simulation) {
    sim.ajouter_chassis(Forme::Sol(SolHorizontal { hauteur: 0.001 }));
    sim.ajouter_chassis(Forme::DemiDroiteVerticale(DemiDroiteVerticaleBasse { point: Vecteur { x: 0., y: 0.24 } }));
    sim.ajouter_chassis(Forme::DemiDroiteVerticale(DemiDroiteVerticaleBasse { point: Vecteur { x: 0.28, y: 0.24 } }));
}

fn ajouter_grille(sim: &mut Simulation) {
    for i in 1..7 {
        sim.ajouter_chassis(Forme::DemiDroiteVerticale(DemiDroiteVerticaleBasse { point: Vecteur { x: 0.04 * i as f32, y: 0.24 } }));
    }
}

fn demo1(sim: &mut Simulation) {
    ajouter_cadre(sim);
    let mut rng = rand::thread_rng();
    for i in 0..6 {
        for j in 0..7 {
            if !rng.gen_bool(0.7) {
                continue;
            }
            let x = 0.02 + j as f32 * 0.04 + 0.001 * rng.gen::<f32>();
            let y = 0.02 + i as f32 * 0.10 + 0.001 * rng.gen::<f32>();
            ajouter_pion(sim, sim.pions.len(), x, y, rng.gen_bool(0.5));
        }
    }
}

fn demo2(sim: &mut Simulation) {
    ajouter_cadre(sim);
    ajouter_pion(sim, 0, 0.10, 0.02, true);
    ajouter_pion(sim, 0, 0.13, 0.30, false);
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum Etat {
    Tour,
    DebutTour,
    DebutPartie(bool),
    AttenteTour(usize),
    Pause,
    Defaire,
}

fn main() {
    let pas = 1./10000.;
    let modele = ModelePion {
        m: 0.050,
        f: 0.1,
        e: 0.3,
        a: 0.1,
        k: (PI / (50. * pas)).powi(2) * 0.050,
    };
    let mut sim = Simulation::nouveau(pas, modele);
    let mut ecran = Ecran::nouveau("Cascad4", 720, 480);

    //demo1(&mut sim);
    //demo2(&mut sim);
    
    ajouter_cadre(&mut sim);
    ajouter_grille(&mut sim);

    let mut fps = FPSManager::new();
    let _ = fps.set_framerate(50);

    let mut etat = Etat::Tour;
    let mut tour = 0;
    let mut pion_suivant = Pion {
        id: tour as usize,
        joueur: Joueur::from(tour % 2 == 0),
        rayon: 0.0195,
        position: Point::cartesien(0.14, 0.26),
        vitesse: Vecteur::NUL,
    };
    let mut evenements = ecran.evenements();

    let mut chassis = sim.chassis.clone();

    'boucle_principale: loop {
        let pions = sim.pions.clone();
        let simu;
        simu = std::thread::spawn(move || {
            if etat != Etat::Pause {
                sim.etapes(200);
            }
            sim
        });

        for ev in evenements.poll_iter() {
            match ev {
                Event::Quit {..} | Event::KeyDown { keycode: Some(Keycode::Escape), .. } => break 'boucle_principale,
                Event::MouseMotion { x, y, .. } => pion_suivant.position.x = ecran.repere().importer(x, y).x.clamp(0.02, 0.26),
                Event::MouseButtonDown { mouse_btn: MouseButton::Left, x, y, .. } => {
                    pion_suivant.position.x = ecran.repere().importer(x, y).x.clamp(0.02, 0.26);
                    if etat == Etat::Tour {
                        etat = Etat::DebutTour;
                    }
                },
                Event::KeyDown { keycode: Some(Keycode::Space), .. } => {
                    etat = Etat::DebutPartie(true);
                },
                Event::KeyDown { keycode: Some(Keycode::Return), .. } => {
                    etat = Etat::DebutPartie(false);
                },
                Event::KeyDown { keycode: Some(Keycode::Down), .. } => {
                    if etat == Etat::Tour {
                        etat = Etat::DebutTour;
                    }
                },
                Event::KeyDown { keycode: Some(Keycode::U), .. } => {
                    if etat == Etat::Tour {
                        etat = Etat::Defaire;
                    }
                },
                _ => {},
            }
        }
        if evenements.keyboard_state().is_scancode_pressed(Scancode::Left) {
            pion_suivant.position.x = (pion_suivant.position.x - 0.003).clamp(0.02, 0.26);
        }
        if evenements.keyboard_state().is_scancode_pressed(Scancode::Right) {
            pion_suivant.position.x = (pion_suivant.position.x + 0.003).clamp(0.02, 0.26);
        }

        ecran.nettoyer();
        dessiner_formes(ecran.canvas(), &chassis, ecran.repere(), Color::BLACK);
        dessiner_pions(ecran.canvas(), &pions, ecran.repere());
        if etat == Etat::Tour {
            dessiner_pion_suivant(ecran.canvas(), &pion_suivant, ecran.repere());
        }
        dessiner_alignements(ecran.canvas(), &pions, &alignements(&pions, 1.3, 0.22 * PI, 0.25 * PI), ecran.repere());
        ecran.actualiser();

        sim = simu.join().unwrap();

        match etat {
            Etat::DebutTour => {
                sim.ajouter_pion(pion_suivant.clone());
                tour += 1;
                pion_suivant.id = tour as usize;
                pion_suivant.joueur = Joueur::from(tour % 2 == 0);
                etat = Etat::AttenteTour(10);
            },
            Etat::DebutPartie(grille) => {
                sim.pions = vec![];
                sim.chassis = vec![];
                ajouter_cadre(&mut sim);
                if grille {
                    ajouter_grille(&mut sim);
                }

                tour = 0;
                pion_suivant.id = tour as usize;
                pion_suivant.joueur = Joueur::from(tour % 2 == 0);
                etat = Etat::Tour;
                chassis = sim.chassis.clone();
            },
            Etat::AttenteTour(delai) => {
                if delai > 0 {
                    etat = Etat::AttenteTour(delai - 1);
                } else {
                    etat = Etat::Tour;
                }
            },
            Etat::Defaire => {
                if tour > 0 {
                    sim.retirer_pion(tour - 1);
                    tour -= 1;
                    pion_suivant.id = tour as usize;
                    pion_suivant.joueur = Joueur::from(tour % 2 == 0);
                }
                etat = Etat::Tour;
            },
            _ => {},
        }

        fps.delay();
    }
    
    /*
    let now = std::time::Instant::now();
    sim.etapes(20000);
    println!("Temps moyen: {} µs", (now.elapsed() / 20000).as_micros());
    println!("Itérations par seconde maximales: {}", 20000. / now.elapsed().as_secs_f32());
    */
}
