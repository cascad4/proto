use super::{vecteur::{Point, Vecteur}, joueur::Joueur, forme::{Forme, Cercle}};

#[derive(Debug, Clone)]
pub struct Pion {
    pub id: usize,
    pub joueur: Joueur,

    pub position: Point,
    pub vitesse: Vecteur,

    pub rayon: f32,
}

impl Pion {
    pub fn forme(&self) -> Forme {
        Forme::Cercle(Cercle { centre: self.position, rayon: self.rayon })
    }
}
