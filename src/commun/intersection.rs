use crate::commun::vecteur::{Vecteur, distance};

use super::forme::{Cercle, DemiDroiteVerticaleBasse, Forme, SolHorizontal};

#[derive(Debug, Clone)]
pub struct PointIntersection {
    pub normale: Vecteur,
    pub enfoncement: f32,
}

impl std::ops::Neg for PointIntersection {
    type Output = Self;

    fn neg(self) -> Self::Output {
        PointIntersection {
            normale: - self.normale,
            enfoncement: self.enfoncement,
        }
    }
}

#[derive(Debug, Clone)]
pub enum Intersection {
    Ponctuelle(PointIntersection),
    Vide,
}

impl Intersection {
    pub fn inverse(&self) -> Intersection {
         match self {
            Intersection::Ponctuelle(p) => Intersection::Ponctuelle(-p.clone()),
            Intersection::Vide => Intersection::Vide,
         }
    }

    pub fn non_vide(&self) -> bool { 
        match self {
            Intersection::Vide => false,
            _ => true,
        }
    }
}

pub fn intersection(f1: &Forme, f2: &Forme) -> Intersection {
    match (f1, f2) {
        (Forme::Cercle(c1), Forme::Cercle(c2)) => intersection_cerles(c1, c2),
        (Forme::Cercle(c1), Forme::DemiDroiteVerticale(d2)) => intersection_cercle_demidroite(c1, d2),
        (Forme::DemiDroiteVerticale(d1), Forme::Cercle(c2)) => intersection_cercle_demidroite(c2, d1).inverse(),
        (Forme::Cercle(c1), Forme::Sol(s2)) => intersection_cercle_sol(c1, s2),
        (Forme::Sol(s1), Forme::Cercle(c2)) => intersection_cercle_sol(c2, s1).inverse(),
        _ => Intersection::Vide,
    }
}

fn intersection_cerles(f1: &Cercle, f2: &Cercle) -> Intersection {
    let dmin = f1.rayon + f2.rayon;
    let d = distance(f1.centre, f2.centre);

    if d <= dmin {
        Intersection::Ponctuelle(
            PointIntersection {
                normale: (f1.centre - f2.centre).unitaire(),
                enfoncement: (dmin - d).abs(),
            }
        )
    } else {
        Intersection::Vide
    }
}

fn intersection_cercle_demidroite(f1: &Cercle, f2: &DemiDroiteVerticaleBasse) -> Intersection {
    if f1.centre.y < f2.point.y {
        let d = f1.centre.x - f2.point.x;
        if -f1.rayon < d && d < 0. {
            Intersection::Ponctuelle(
                PointIntersection {
                    normale: Vecteur { x: -1., y: 0. },
                    enfoncement: f1.rayon + d,
                }
            )
        } else if 0. < d && d < f1.rayon {
            Intersection::Ponctuelle(
                PointIntersection {
                    normale: Vecteur { x: 1., y: 0. },
                    enfoncement: f1.rayon - d,
                }
            )
        } else {
            Intersection::Vide
        }
    } else {
        intersection_cerles(f1, &Cercle { centre: f2.point, rayon: 0. })
    }
}

fn intersection_cercle_sol(f1: &Cercle, f2: &SolHorizontal) -> Intersection {
    if f1.centre.y < f2.hauteur + f1.rayon {
        Intersection::Ponctuelle(
            PointIntersection {
                normale: Vecteur { x: 0., y: 1. },
                enfoncement: f2.hauteur + f1.rayon - f1.centre.y,
            }
        )
    } else {
        Intersection::Vide
    }
}

