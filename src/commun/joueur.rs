#[derive(Debug, Clone, Copy)]
pub enum Joueur {
    J1,
    J2,
}

impl std::cmp::PartialEq for Joueur {
    fn eq(&self, autre: &Self) -> bool {
        match (self, autre) {
            (Joueur::J1, Joueur::J1) => true,
            (Joueur::J2, Joueur::J2) => true,
            _ => false,
        }
    }

    fn ne(&self, autre: &Self) -> bool {
        match (self, autre) {
            (Joueur::J1, Joueur::J2) => true,
            (Joueur::J2, Joueur::J1) => true,
            _ => false,
        }
    }
}

impl From<bool> for Joueur {
    fn from(valeur: bool) -> Self {
        if valeur {
            Joueur::J1
        } else {
            Joueur::J2
        }
    }
}
