use std::{ops::{Add, Neg, Sub, Mul, Div, AddAssign}, f32::consts::PI};

pub type Point = Vecteur; 

pub trait IntoF32 {
    fn into_f32(&self) -> f32;
}

impl IntoF32 for f32 {
    fn into_f32(&self) -> f32 {
        *self
    }
}

impl IntoF32 for f64 {
    fn into_f32(&self) -> f32 {
        *self as f32
    }
}

impl IntoF32 for u32 {
    fn into_f32(&self) -> f32 {
        *self as f32
    }
}

impl IntoF32 for i32 {
    fn into_f32(&self) -> f32 {
        *self as f32
    }
}

impl IntoF32 for usize {
    fn into_f32(&self) -> f32 {
        *self as f32
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Vecteur {
    pub x: f32,
    pub y: f32,
}

impl Vecteur {
    pub const NUL: Vecteur = Vecteur { x: 0., y: 0. };

    pub fn cartesien<U, V>(x: U, y: V) -> Vecteur where U: IntoF32, V: IntoF32 {
        Vecteur {
            x: x.into_f32(),
            y: y.into_f32(),
        }
    }

    pub fn trigo<U, V>(r: U, theta: V) -> Vecteur where U: IntoF32, V: IntoF32 {
        Vecteur {
            x: r.into_f32() * theta.into_f32().cos(),
            y: r.into_f32() * theta.into_f32().sin(),
        }
    }
    
    pub fn orthogonal(&self) -> Vecteur {
        Vecteur { 
            x: - self.y,
            y: self.x,
        }
    }

    pub fn unitaire(&self) -> Vecteur {
        *self / self.norme()
    }

    pub fn orthonorme(&self) -> Vecteur {
        self.orthogonal().unitaire()
    }

    pub fn norme(&self) -> f32 {
        (self.x * self.x + self.y * self.y).sqrt()
    }

    pub fn norme_inf(&self) -> f32 {
        f32::max(self.x.abs(), self.y.abs())
    }

    pub fn norme_x(&self, x: f32) -> f32 {
        (self.x.abs().powf(x) + self.y.abs().powf(x)).powf(1. / x)
    }

    pub fn argument(&self) -> f32 {
        if self.x > 0. {
            (self.y / self.x).atan()
        } else if self.x < 0. {
            (self.y / self.x).atan() + PI
        } else if self.y > 0.{
            PI / 2.
        } else if self.y < 0. {
            - PI / 2.
        } else {
            f32::NAN
        }
    }
}

impl Add<Vecteur> for Vecteur {
    type Output = Vecteur;

    fn add(self, autre: Vecteur) -> Self::Output {
        Vecteur {
            x: self.x + autre.x,
            y: self.y + autre.y,
        }
    }
}

impl Neg for Vecteur {
    type Output = Vecteur;

    fn neg(self) -> Self::Output { 
        Vecteur {
            x: - self.x,
            y: - self.y,
        }
    }
}

impl Sub<Vecteur> for Vecteur {
    type Output = Vecteur;

    fn sub(self, autre: Vecteur) -> Self::Output {
        Vecteur {
            x: self.x - autre.x,
            y: self.y - autre.y,
        }
    }
}

impl Mul<f32> for Vecteur {
    type Output = Vecteur;

    fn mul(self, autre: f32) -> Self::Output {
        Vecteur {
            x: self.x * autre,
            y: self.y * autre,
        }
    }
}

impl Mul<Vecteur> for f32 {
    type Output = Vecteur;

    fn mul(self, autre: Vecteur) -> Self::Output {
        Vecteur {
            x: self * autre.x,
            y: self * autre.y,
        }
    }
}

impl Div<f32> for Vecteur {
    type Output = Vecteur;

    fn div(self, autre: f32) -> Self::Output {
        Vecteur {
            x: self.x / autre,
            y: self.y / autre,
        }
    }
}

impl AddAssign for Vecteur {
    fn add_assign(&mut self, autre: Self) {
        self.x += autre.x;
        self.y += autre.y;
    }
}

pub fn distance(v1: Vecteur, v2: Vecteur) -> f32 {
    (v2 - v1).norme()
}

pub fn angle(v1: Vecteur, v2: Vecteur) -> f32 {
    v2.argument() - v1.argument()
}

pub fn ps(v1: Vecteur, v2: Vecteur) -> f32 {
    v1.x * v2.x + v1.y * v2.y
}
