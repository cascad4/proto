use crate::commun::{vecteur::Point, objet::Pion};


#[derive(Debug, Clone)]
pub struct Cercle {
    pub centre: Point,
    pub rayon: f32,
}

#[derive(Debug, Clone)]
pub struct DemiDroiteVerticaleBasse {
    pub point: Point,
}

#[derive(Debug, Clone)]
pub struct SolHorizontal {
    pub hauteur: f32,
}

#[derive(Debug, Clone)]
pub enum Forme {
    Cercle(Cercle),
    DemiDroiteVerticale(DemiDroiteVerticaleBasse),
    Sol(SolHorizontal),
}
