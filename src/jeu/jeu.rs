use std::{f32::consts::PI, sync::mpsc::{Receiver, Sender}};

use sdl2::{EventPump, pixels::Color, keyboard::{Scancode, Keycode, Mod}, event::Event, mouse::MouseButton};

use crate::{simulation::simulation::Simulation, interface::{affichage::Ecran, dessiner::{dessiner_pion_suivant, dessiner_alignements, dessiner_pions, dessiner_formes, dessiner_trace}}, commun::{objet::Pion, forme::Forme, joueur::Joueur}};

use super::{parallele::{CommandeJeu, InformationJeu, fil_simulation}, utilitaire::{nouveau_pion_suivant, nouveau_modele}, alignement::alignements, trace::Trace, fin::Avancement};

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Etat {
    Tour,
    Attente(usize),
    Pause,
}

pub struct Jeu {
    etat: Etat,
   
    emmeteur: Sender<CommandeJeu>,
    recepteur: Receiver<InformationJeu>,

    tour: usize,
    pion_suivant: Pion,

    pions: Vec<Pion>,
    chassis: Vec<Forme>,

    avancement: Avancement,
    traces: Vec<Trace>,
}

impl Jeu {
    pub fn nouveau() -> Jeu {
        let (emmeteur, recepteur_distant) = std::sync::mpsc::channel();
        let (emmeteur_distant, recepteur) = std::sync::mpsc::channel();

        fil_simulation(
            Simulation::nouveau(1e-4, nouveau_modele()),
            recepteur_distant,
            emmeteur_distant,
        );

        Jeu {
            etat: Etat::Tour,

            emmeteur,
            recepteur,

            tour: 0,
            pion_suivant: nouveau_pion_suivant(),

            pions: vec![],
            chassis: vec![],

            avancement: Avancement::nouveau(50),
            traces: vec![],
        }
    }

    pub fn nouvelle_partie(&mut self, grille: bool) {
        self.tour = 0;

        self.pion_suivant.id = self.tour;
        self.pion_suivant.joueur = Joueur::from(self.tour % 2 == 0);

        let _ = self.emmeteur.send(CommandeJeu::RetirerPions);
        
        if grille {
            let _ = self.emmeteur.send(CommandeJeu::MettreGrille);
        } else {
            let _ = self.emmeteur.send(CommandeJeu::MettreCadre);
        }
        
        let _ = self.emmeteur.send(CommandeJeu::EnvoyerChassis);
        
        match self.recepteur.recv().unwrap() {
            InformationJeu::Chassis(chassis) => self.chassis = chassis,
            _ => unreachable!(),
        }
        
        self.avancement.reinitialiser();
        self.traces.clear()
    }

    pub fn deplacer_pion_suivant(&mut self, x: f32) {
        self.pion_suivant.position.x = x.clamp(0.02, 0.26);
    }

    pub fn jouer_pion_suivant(&mut self, trace: bool) {
        let _ = self.emmeteur.send(CommandeJeu::AjouterPion(self.pion_suivant.clone()));
        
        if trace {
            self.traces.clear();
            
            self.traces.push(Trace::nouveau(&self.pion_suivant));
            for p in &self.pions {
                self.traces.push(Trace::nouveau(p));
            }
        }

        self.tour += 1;
        
        self.pion_suivant.id = self.tour;
        self.pion_suivant.joueur = Joueur::from(self.tour % 2 == 0);
    }

    pub fn annuler_tour(&mut self) {
        if self.tour > 0 {
            self.tour -= 1;

            let _ = self.emmeteur.send(CommandeJeu::RetirerPion(self.tour));

            self.pion_suivant.id = self.tour;
            self.pion_suivant.joueur = Joueur::from(self.tour % 2 == 0);
        }

        self.avancement.reinitialiser();
    }

    pub fn valider_tour(&mut self) {
        if self.tour > 0 {
            let p = &self.pions[self.tour - 1].position;
            if !(0. < p.x && p.x < 0.28 && 0. < p.y && p.y < 0.24) {
                self.annuler_tour();
            }
        }
    }

    pub fn actualiser(&mut self) {
        if self.etat != Etat::Pause {
            let _ = self.emmeteur.send(CommandeJeu::EnvoyerPions);
            match self.recepteur.recv().unwrap() {
                InformationJeu::Pions(pions) => self.pions = pions,
                _ => unreachable!(),
            }
        }
        match self.etat {
            Etat::Attente(delai) => {
                if delai > 0 {
                    self.etat = Etat::Attente(delai - 1);
                } else {
                    self.etat = Etat::Tour;
                    self.valider_tour();
                }
            },
            _ => {},
        }

        if self.etat != Etat::Pause {
            let _ = self.emmeteur.send(CommandeJeu::Simuler);
        }

        self.avancement.actualiser(alignements(&self.pions, 1.3, 0.22 * PI, 0.25 * PI));

        if self.etat != Etat::Pause {
            for trace in &mut self.traces {
                for p in &self.pions {
                    if p.id == trace.id {
                        trace.actualiser(p);
                    }
                }
            }
        }
    }

    pub fn actualiser_graphique(&mut self, ecran: &Ecran) -> bool {
        self.actualiser();
        
        for trace in &mut self.traces {
            dessiner_trace(ecran.canvas(), trace, ecran.repere());
        }

        dessiner_formes(ecran.canvas(), &self.chassis, ecran.repere(), Color::BLACK);
        dessiner_pions(ecran.canvas(), &self.pions, ecran.repere());
        dessiner_alignements(ecran.canvas(), &self.pions, &self.avancement.alignements, ecran.repere());
        
        if self.etat == Etat::Tour {
            dessiner_pion_suivant(ecran.canvas(), &self.pion_suivant, ecran.repere());
        }

        let mut evenements = ecran.contexte.event_pump().unwrap();
        for ev in evenements.poll_iter() {
            match ev {
                Event::Quit {..} | Event::KeyDown { keycode: Some(Keycode::Escape | Keycode::Q), .. } => {
                    let _ = self.emmeteur.send(CommandeJeu::Arret);
                    return false;
                },
                Event::MouseMotion { x, y, .. } => {
                    if self.etat == Etat::Tour {
                        self.deplacer_pion_suivant(ecran.repere().importer(x, y).x);
                    }
                },
                Event::MouseButtonDown { mouse_btn, x, y, .. } => {
                    if self.etat == Etat::Tour {
                        self.deplacer_pion_suivant(ecran.repere().importer(x, y).x);
                        self.jouer_pion_suivant(mouse_btn == MouseButton::Right);
                        self.etat = Etat::Attente(10);
                    }
                },
                Event::KeyDown { keycode: Some(Keycode::Return), keymod, .. } => {
                    self.nouvelle_partie(keymod == Mod::LSHIFTMOD);
                    self.etat = Etat::Tour;
                },
                Event::KeyDown { keycode: Some(Keycode::Down), keymod, .. } => {
                    if self.etat == Etat::Tour {
                        self.jouer_pion_suivant(keymod == Mod::LCTRLMOD);
                        self.etat = Etat::Attente(10);
                    }
                },
                Event::KeyDown { keycode: Some(Keycode::U), .. } => {
                    self.annuler_tour();
                },
                Event::KeyDown { keycode: Some(Keycode::C), .. } => {
                    self.traces.clear();
                },
                Event::KeyDown { keycode: Some(Keycode::Space), .. } => {
                    if self.etat == Etat::Pause {
                        self.etat = Etat::Attente(10);
                    } else {
                        self.etat = Etat::Pause;
                    }
                },
                _ => {},
            }
        }
        if evenements.keyboard_state().is_scancode_pressed(Scancode::Left) {
            self.pion_suivant.position.x = (self.pion_suivant.position.x - 0.003).clamp(0.02, 0.26);
        }
        if evenements.keyboard_state().is_scancode_pressed(Scancode::Right) {
            self.pion_suivant.position.x = (self.pion_suivant.position.x + 0.003).clamp(0.02, 0.26);
        }

        true
    }

    pub fn partie_finie(&self) -> bool {
        self.avancement.partie_finie(&self.pions)
    }

    pub fn resultat(&self) -> super::fin::Resultat {
        self.avancement.resultat(&self.pions)
    }

    pub fn initialiser(&mut self) {
        let _ = self.emmeteur.send(CommandeJeu::MettreCadre);
        let _ = self.emmeteur.send(CommandeJeu::EnvoyerChassis);
        match self.recepteur.recv().unwrap() {
            InformationJeu::Chassis(chassis) => self.chassis = chassis,
            _ => unreachable!(),
        }
    }
}
