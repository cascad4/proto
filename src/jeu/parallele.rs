use std::{thread::JoinHandle, sync::mpsc::{Receiver, Sender}};

use crate::{commun::{objet::Pion, forme::Forme}, simulation::simulation::Simulation};

use super::utilitaire::{mettre_cadre, mettre_grille};


#[derive(Debug, Clone)]
pub enum InformationJeu {
    Pions(Vec<Pion>),
    Chassis(Vec<Forme>),
}

#[derive(Debug)]
pub enum CommandeJeu {
    Simuler,
    AjouterPion(Pion),
    RetirerPion(usize),
    RetirerPions,
    EnvoyerPions,
    MettreCadre,
    MettreGrille,
    EnvoyerChassis,
    Arret,
}

pub fn fil_simulation(mut sim: Simulation, recepteur: Receiver<CommandeJeu>, emmeteur: Sender<InformationJeu>) -> JoinHandle<()> {
    std::thread::spawn(move || {
        'boucle_simulation: loop {
            match recepteur.recv().unwrap_or(CommandeJeu::Arret) {
                CommandeJeu::Simuler => sim.etapes(200),
                CommandeJeu::Arret => break 'boucle_simulation,

                CommandeJeu::AjouterPion(pion) => sim.ajouter_pion(pion),
                CommandeJeu::RetirerPion(id) => {
                    sim.retirer_pion(id);
                },
                CommandeJeu::RetirerPions => {
                    sim.pions.clear();
                },
                CommandeJeu::EnvoyerPions => {
                    let _ = emmeteur.send(InformationJeu::Pions(sim.pions.clone()));
                },

                CommandeJeu::MettreCadre => mettre_cadre(&mut sim),
                CommandeJeu::MettreGrille => mettre_grille(&mut sim),
                CommandeJeu::EnvoyerChassis => {
                    let _ = emmeteur.send(InformationJeu::Chassis(sim.chassis.clone()));
                },
            }
        }
    })
}
