use std::f32::consts::PI;

use crate::commun::objet::Pion;

type MatriceAdjacence = Vec<Vec<f32>>;
pub type Alignement = Vec<usize>;

fn angle_principal(a: f32) -> f32 {
    (a + PI) % (2. * PI) - PI
}

fn angle_majore(a: f32, max: f32) -> bool {
    let a = angle_principal(a);

    -max <= a && a <= max
}

struct Successeur {
    pub successeur: usize,
    pub chemin: Vec<usize>,
}

fn longueur_chemin(succ: &Successeur) -> usize {
    succ.chemin.len()
}

fn angle_chemin(succ: &Successeur, adj: &MatriceAdjacence) -> f32 {
    assert_eq!(longueur_chemin(succ), 4);

    adj[succ.chemin[2]][succ.chemin[3]] - adj[succ.chemin[0]][succ.chemin[1]]
}

fn sont_voisins(p1: &Pion, p2: &Pion, dmax: f32) -> bool {
    (p2.position - p1.position).norme_x(5.) < dmax && p1.joueur == p2.joueur
}

fn angle_voisins(p1: &Pion, p2: &Pion) -> f32 {
    (p2.position - p1.position).argument()
}

fn matrice_adjacence(pions: &Vec<Pion>, dmax: f32) -> Vec<Vec<f32>> {
    let n = pions.len();

    let mut adj = vec![vec![f32::NAN; n]; n];

    for i in 0..n {
        for j in 0..n {
            if i != j && sont_voisins(&pions[i], &pions[j], dmax) {
                adj[i][j] = angle_voisins(&pions[i], &pions[j]);
                //adj[j][i] = adj[i][j] + PI;
            }
        }
    }

    adj
}

fn successeurs(predecesseur: usize, adj: &MatriceAdjacence) -> Vec<Successeur> {
    let n = adj.len();
    let mut successeurs = vec![];
    
    for i in 0..n {
        if !adj[predecesseur][i].is_nan() {
            successeurs.push(
                Successeur {
                    successeur: i,
                    chemin: vec![i, predecesseur],
                }
            );
        }
    }

    successeurs
}

fn successeurs_alignes(pred: Successeur, adj: &MatriceAdjacence, alpha: f32) -> Vec<Successeur> {
    let n = adj.len();
    let mut successeurs = vec![];

    let p = pred.successeur;
    for i in 0..n {
        if !adj[p][i].is_nan() {
            let angle_depart = adj[p][i];
            let angle_arrivee = adj[pred.chemin[1]][pred.chemin[0]];

            if angle_majore(angle_depart - angle_arrivee, alpha) {
                successeurs.push(
                    Successeur {
                        successeur: i,
                        chemin: [vec![i], pred.chemin.clone()].concat(),
                    }
                );
            }
        }
    }

    successeurs
}

fn alignement(mut succ: Successeur) -> Vec<usize> {
    if succ.chemin[0] < succ.chemin[3] {
        succ.chemin
    } else {
        succ.chemin.reverse();
        succ.chemin
    }
}

pub fn alignements(pions: &Vec<Pion>, eloignement: f32, alpha: f32, beta: f32) -> Vec<Vec<usize>> {
    if pions.len() == 0 {
        return vec![];
    }

    let dmax = pions[0].rayon * 2. * eloignement;

    let adj = matrice_adjacence(pions, dmax);

    let n = pions.len();
    let mut alignements: Vec<Vec<usize>> = vec![];
    for i in 0..n {
        let mut file = successeurs(i, &adj);

        while file.len() > 0 {
            let succ = file.pop().unwrap();
            if longueur_chemin(&succ) < 4 {
                file.append(&mut successeurs_alignes(succ, &adj, alpha));
            } else {
                if angle_majore(angle_chemin(&succ, &adj), beta) {
                    let alignement = alignement(succ);
                    if !alignements.contains(&alignement) {
                        alignements.push(alignement);
                    }
                }
            }
        }
    }

    alignements
}

