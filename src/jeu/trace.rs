use crate::commun::{joueur::Joueur, vecteur::Point, objet::Pion};

#[derive(Debug, Clone)]
pub struct Trace {
    pub id: usize,
    pub joueur: Joueur,
    pub rayon: f32,

    pub positions: Vec<Point>,
}

impl Trace {
    pub fn nouveau(pion: &Pion) -> Trace {
        Trace {
            id: pion.id,
            joueur: pion.joueur.clone(),
            rayon: pion.rayon,

            positions: vec![pion.position],
        }
    }

    pub fn actualiser(&mut self, pion: &Pion) {
        if self.positions.len() < 50 {
            self.positions.push(pion.position);
        }
    }
}
