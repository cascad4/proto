use std::f32::consts::PI;

use crate::{simulation::{modelisation::modele::ModelePion, simulation::Simulation}, commun::{objet::Pion, joueur::Joueur, vecteur::{Point, Vecteur}, forme::{Forme, SolHorizontal, DemiDroiteVerticaleBasse}}};


pub fn nouveau_modele() -> ModelePion {
    ModelePion {
        m: 0.050,
        f: 0.1,
        e: 0.3,
        k: (PI / (50. * 1e-4)).powi(2) * 0.050,
        a: 0.1,
    }
}

pub fn nouveau_pion_suivant() -> Pion {
    Pion {
        id: 0,
        joueur: Joueur::J1,
        position: Point::cartesien(0.14, 0.26),
        vitesse: Vecteur::NUL,
        rayon: 0.0195,
    }
}

fn ajouter_cadre(sim: &mut Simulation) {
    sim.ajouter_chassis(Forme::Sol(SolHorizontal { hauteur: 0.001 }));
    sim.ajouter_chassis(Forme::DemiDroiteVerticale(DemiDroiteVerticaleBasse { point: Vecteur { x: 0., y: 0.24 } }));
    sim.ajouter_chassis(Forme::DemiDroiteVerticale(DemiDroiteVerticaleBasse { point: Vecteur { x: 0.28, y: 0.24 } }));
}

fn ajouter_grille(sim: &mut Simulation) {
    for i in 1..7 {
        sim.ajouter_chassis(Forme::DemiDroiteVerticale(DemiDroiteVerticaleBasse { point: Vecteur { x: 0.04 * i as f32, y: 0.24 } }));
    }
}

pub fn mettre_cadre(sim: &mut Simulation) {
    sim.chassis.clear();
    ajouter_cadre(sim);
}

pub fn mettre_grille(sim: &mut Simulation) {
    sim.chassis.clear();
    ajouter_cadre(sim);
    ajouter_grille(sim);
}
