use crate::commun::{objet::Pion, vecteur::{Vecteur, ps}, intersection::Intersection};

use super::modele::ModelePion;

pub fn poids(modele : &ModelePion) -> Vecteur {
    Vecteur {
        x: 0.,
        y: - modele.m * 9.81,
    }
}

pub fn trainee(modele: &ModelePion, pion: &Pion) -> Vecteur {
    - modele.a * pion.vitesse.norme() * pion.vitesse
}

pub fn contact(modele: &ModelePion, pion1: &Pion, pion2: &Pion, intersection: &Intersection) -> Vecteur {
    match intersection {
        Intersection::Ponctuelle(p) => {
            let tangente = p.normale.orthogonal();

            let vitesse_relative = pion2.vitesse - pion1.vitesse;
            let vitesse_normale = ps(vitesse_relative, p.normale);

            let k = if vitesse_normale < 0. { modele.k * modele.e.powi(2) } else { modele.k };
            let n = k * p.enfoncement;

            let vitesse_tangente = ps(vitesse_relative, tangente);
            let t = if vitesse_tangente < 0. {
                - modele.f * n
            } else {
                modele.f * n
            };

            n * p.normale + t * tangente
        },
        Intersection::Vide => Vecteur::NUL,
    }
}

pub fn contact_chassis(modele: &ModelePion, pion1: &Pion, intersection: &Intersection) -> Vecteur {
    match intersection {
        Intersection::Ponctuelle(p) => {
            let tangente = p.normale.orthogonal();

            let vitesse_relative = - pion1.vitesse;
            let vitesse_normale = ps(vitesse_relative, p.normale);

            let k = if vitesse_normale < 0. { modele.k * modele.e.powi(2) } else { modele.k };
            let n = k * p.enfoncement;

            let vitesse_tangente = ps(vitesse_relative, tangente);
            let t = if vitesse_tangente < 0. {
                - modele.f * n
            } else {
                modele.f * n
            };

            n * p.normale + t * tangente
        },
        Intersection::Vide => Vecteur::NUL,
    }
}

