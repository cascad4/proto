use crate::commun::{objet::Pion, forme::Forme, intersection::{Intersection, intersection}, vecteur::Vecteur};

use super::modelisation::{modele::ModelePion, action_mecanique};

#[derive(Debug, Clone)]
pub struct Simulation {
    pub pions: Vec<Pion>,
    pub modele: ModelePion,
    pub chassis: Vec<Forme>,
    pub pas: f32,
}

impl Simulation {
    pub fn nouveau(pas: f32, modele: ModelePion) -> Simulation {
        Simulation {
            pions: vec![],
            modele,
            chassis: vec![],
            pas,
        }
    }

    pub fn ajouter_pion(&mut self, pion: Pion) {
        self.pions.push(pion);
    }

    pub fn obtenir_pion(&self, id: usize) -> Option<Pion> {
        for i in 0..self.pions.len() {
            if self.pions[i].id == id {
                return Some(self.pions[i].clone());
            }
        }

        None
    }

    pub fn retirer_pion(&mut self, id: usize) -> Option<Pion> {
        for i in 0..self.pions.len() {
            if self.pions[i].id == id {
                return Some(self.pions.remove(i));
            }
        }

        None
    }

    pub fn ajouter_chassis(&mut self, forme: Forme) {
        self.chassis.push(forme);
    }

    pub fn etapes(&mut self, n: usize) {
        for _ in 0..n {
            self.etape();
        }
    }

    pub fn etape(&mut self) {
        let mut intersections = vec![vec![Intersection::Vide; self.pions.len() + self.chassis.len()]; self.pions.len()];
        for i in 0..self.pions.len() {
            let f = self.pions[i].forme();
            for j in i + 1..self.pions.len() {
                let inter = intersection(&f, &self.pions[j].forme());
                intersections[j][i] = inter.inverse();
                intersections[i][j] = inter;
            }
            for j in 0..self.chassis.len() {
                intersections[i][self.pions.len() + j] = intersection(&f, &self.chassis[j]);
            }
        }

        for i in 0..self.pions.len() {
            self.traiter(i, &intersections[i]);
        }
    }

    fn traiter(&mut self, obj: usize, intersections: &Vec<Intersection>) {
        let mut resultante = Vecteur::NUL;

        resultante += action_mecanique::poids(&self.modele);
        resultante += action_mecanique::trainee(&self.modele, &self.pions[obj]);

        for i in 0..self.pions.len() {
            resultante += action_mecanique::contact(&self.modele, &self.pions[obj], &self.pions[i], &intersections[i]);
        }

        for i in 0..self.chassis.len() {
            resultante += action_mecanique::contact_chassis(&self.modele, &self.pions[obj], &intersections[self.pions.len() + i]);
        }

        let acceleration = resultante / self.modele.m;
        let vitesse = self.pions[obj].vitesse + acceleration * self.pas;
        self.pions[obj].position += vitesse * self.pas;
        self.pions[obj].vitesse = vitesse;
    }
}
